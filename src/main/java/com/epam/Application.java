package com.epam;
import java.util.Scanner;

public class Application {
    public static void main(String[] args) {
        final int att = 100;
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter number of guests at the party: ");
        System.out.println("number should be higher than '2':");
        int number = scanner.nextInt();
        int countTimes= 0;
        int Reached = 0;
        for (int i = 0; i < att; i++) {
            boolean guests[] = new boolean[number];
            guests[1] = true;
            boolean alreadyKnow = false;
            int nextPerson = -1;
            int currentPerson = 1;
            while (!alreadyKnow) {
                nextPerson = 1 + (int) (Math.random() * (number - 1));
                if (nextPerson == currentPerson) {
                    while (nextPerson == currentPerson)
                        nextPerson = 1 + (int) (Math.random() * (number - 1));
                }
                if (guests[nextPerson])
                {
                    if (rumorSpreaded(guests))
                        countTimes++;
                    Reached = Reached + countPeopleReached(guests);
                    alreadyKnow = true;
                }
                guests[nextPerson] = true;
                currentPerson = nextPerson;
            }
        }
        System.out.println("Chances that everyone except Alice will hear rumor in " + att + " attempts: " +
                (double) countTimes / att);
        System.out.println("Average amount of guests that will hear the rumor is: " + Reached / att);
    }

    public static int countPeopleReached(boolean arr[]) {
        int counter = 0;
        for (int i = 1; i < arr.length; i++)
            if (arr[i])
                counter++;
        return counter;
    }

    public static boolean rumorSpreaded(boolean arr[]) {
        for (int i = 1; i < arr.length; i++)
            if (!arr[i])
                return false;
        return true;
    }
}